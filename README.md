# Introduction

If you apply to Play SQL, we'll do a coding test. Please follow the instructions given in the PDF.

# License and source

This is an extraction of https://github.com/dropwizard/dropwizard/tree/master/dropwizard-example , with a modifications for the purpose of the coding test.